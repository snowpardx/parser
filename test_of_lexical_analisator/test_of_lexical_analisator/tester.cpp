#include "tester.h"

bool test::is_equal_to_lex_conv( std::istream&stream,const char*s ){
	char mass[45];
	std::string str;
	stream.getline(mass,40);
	str=mass;
	while(*(str.end()-1)!=')'){
		stream.getline(mass,40);
		str+="\n";
		str+=mass;
	}
	if (str==s)
		return true;
	return false;
}

void tester::run(const char *AppName, const char *namein,const char*nameout){
	std::string s=AppName; s+=" "; s+=namein; s+=" "; s+=nameout;
	system(s.c_str());
	std::ifstream stream;
	stream.open(nameout,std::ios::in);
	run_tests(stream);
}


void tester::run_tests(std::istream&stream){
	char non_use[80];
	for(int i=0;i<4;i++)
		stream.getline(non_use,75);
	for(auto it:this->tests)
		if (it->run(stream))
			std::cout<<"\n\nOK\n\n";
		else
			std::cout<<"\n\nNOT OK\n\n";
}

void tester::add( test&t ){
	this->tests.push_back(&t);
}

std::string tester::get_ful_path( std::string a,std::string b ){
	for (auto it=a.end()-1;*it!='\\';it--)
		a.erase(it);
	return a+b;
}

bool test1::run(std::istream&stream){
	std::cout<<"Proverka na to chto //TEST raspoznaetsa kak neraspoznanaya\n \
lexema // i identificator TEST";
	if (!is_equal_to_lex_conv(stream,"(1, //, unknown_lexems, 6)"))
		return false;
	if (!is_equal_to_lex_conv(stream, "(1, TEST, identificators, 1)" ))
		return false;
	return true;
}

bool test2::run( std::istream&stream ){
	std::cout<<"Proverka na to chto #include\"test.h\" raspoznaetsa kak\n \
neraspoznanaya lexema #,neraspoznanaya lexema include\n\
i stroka \"test.h\"";
	if (!is_equal_to_lex_conv(stream,"(2, #, unknown_lexems, 2)"))
		return false;
	if (!is_equal_to_lex_conv(stream, "(2, include, identificators, 3)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(2, \"test.h\", strings, 6)"))
		return false;
	return true;
}

bool test3::run( std::istream&stream ){
	std::cout<<"Proverka na to chto int main(){ raspoznaetsa\n\
kak kluchevoe slovo int, identificator main,\n\
razdelitel (, razdelitel ) i razdelitel {";
	if (!is_equal_to_lex_conv(stream,"(3, int, keywords, 1)"))
		return false;
	if (!is_equal_to_lex_conv(stream, "(3, main, identificators, 3)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(3, (, delimiters, 7)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(3, ), delimiters, 8)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(3, {, delimiters, 2)" ))
		return false;
	return true;
}

bool test4::run( std::istream&stream ){
	std::cout<<"Proverka na to chto 	char str[5];  (probeli-elementi\n \
testirovaniya) raspoznaetsa kak kluchevoe\n\
slovo char, identificator str,\n\
 razdelitel ], constanta 5, razdelitel ]\n\
 i razdelitel ;";
	if (!is_equal_to_lex_conv(stream,"(4, char, keywords, 7)"))
		return false;
	if (!is_equal_to_lex_conv(stream, "(4, str, identificators, 4)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(4, [, delimiters, 3)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(4, 5, constants, 9)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(4, ], delimiters, 5)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(4, ;, delimiters, 4)" ))
		return false;
	return true;
}

bool test5::run( std::istream&stream ){
	std::cout<<"Proverka na to chto OooooooochenDlinniyIdentificator\n\
rasposnaetsa kak neskolko identificatorov\n\
(dlinnoy ne bolee 8) a imenno Oooooooo,\n \
chenDlin, niyIdent, ificator";
	if (!is_equal_to_lex_conv(stream,"(5, Oooooooo, identificators, 9)"))
		return false;
	if (!is_equal_to_lex_conv(stream, "(5, chenDlin, identificators, 2)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(5, niyIdent, identificators, 0)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(5, ificator, identificators, 2)" ))
		return false;
	return true;
}

bool test6::run( std::istream&stream ){
	std::cout<<"Proverka na to chto qwerty12345 rasposnaetsa kak \n\
identificator(dlinnoy ne bolee 8) qwerty12\
i konstanta 345";
	if (!is_equal_to_lex_conv(stream,"(6, qwerty12, identificators, 2)"))
		return false;
	if (!is_equal_to_lex_conv(stream, "(6, 345, constants, 2)" ))
		return false;
	return true;
}

bool test7::run( std::istream&stream ){
	std::cout<<"Proverka na to chto 12345������atoi rasposnaetsa\n\
kak konstanta 12345,neraspoznanaya\n \
lexema ������ i kluchevoe slovo atoi";
	if (!is_equal_to_lex_conv(stream,"(7, 12345, constants, 2)"))
		return false;
	if (!is_equal_to_lex_conv(stream, "(7, ������, unknown_lexems, 4)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(7, atoi, keywords, 0)" ))
		return false;
	return true;
}

bool test8::run( std::istream&stream ){
	std::cout<<"Proverka na to chto 123456789123456789 hoty i ochen dlinnoe\n\
rasposnaetsa kak konstanta";
	if (!is_equal_to_lex_conv(stream,"(8, 123456789123456789, constants, 8)"))
		return false;
	return true;
}

bool test9::run( std::istream&stream ){
	std::cout<<"Proverka na to chto for(int i=0;i<5;) rasposnaetsa kak\n\
identificator for, razdelitel (, \n\
kluchevoe slovo int, identificator i,\n \
razdelitel =, konstanta 0, razdelitel ;,\n\
identificator i, neraspoznanaya lexema <,\n\
konstanta 5, razdelitel ; i razdelitel )";
	if (!is_equal_to_lex_conv(stream,"(9, for, identificators, 8)"))
		return false;
	if (!is_equal_to_lex_conv(stream, "(9, (, delimiters, 7)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(9, int, keywords, 1)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(9, i, identificators, 6)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(9, =, delimiters, 6)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(9, 0, constants, 4)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(9, ;, delimiters, 4)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(9, i, identificators, 6)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(9, <, unknown_lexems, 5)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(9, 5, constants, 9)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(9, ;, delimiters, 4)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(9, ), delimiters, 8)" ))
		return false;
	return true;
}

bool test10::run( std::istream&stream ){
	std::cout<<"Proverka na to chto str[i]=++i; rasposnaetsa kak \n\
identificator str, razdelitel [, identificator i,\n\
razdelitel ], razdelitel =, razdelitel +, \n\
razdelitel +, identificator i i razdelitel ;";
	if (!is_equal_to_lex_conv(stream,"(10, str, identificators, 4)"))
		return false;
	if (!is_equal_to_lex_conv(stream, "(10, [, delimiters, 3)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(10, i, identificators, 6)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(10, ], delimiters, 5)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(10, =, delimiters, 6)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(10, +, delimiters, 10)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(10, +, delimiters, 10)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(10, i, identificators, 6)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(10, ;, delimiters, 4)" ))
		return false;
	return true;
}

bool test11::run( std::istream&stream ){
	std::cout<<"Proverka na to chto int a=atoi(str); rasposnaetsa \n\
kak kluchevoe slovo int, identificator a, razdelitel =\n\
kluchevoe slovo atoi, razdelitel (,\n \
identificator str, razdelitel ) i razdelitel ;";
	if (!is_equal_to_lex_conv(stream, "(11, int, keywords, 1)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(11, a, identificators, 9)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(11, =, delimiters, 6)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(11, atoi, keywords, 0)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(11, (, delimiters, 7)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(11, str, identificators, 4)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(11, ), delimiters, 8)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(11, ;, delimiters, 4)" ))
		return false;
	return true;
}

bool test12::run( std::istream&stream ){
	std::cout<<"Proverka na to chto string s = itoa(a); rasposnaetsa kak \n\
identificator string, identificator s, razdelitel =\n\
kluchevoe slovo itoa, razdelitel (, identificator a,\n\
razdelitel ) i razdelitel ;";
	if (!is_equal_to_lex_conv(stream, "(12, string, identificators, 3)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(12, s, identificators, 5)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(12, =, delimiters, 6)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(12, itoa, keywords, 0)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(12, (, delimiters, 7)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(12, a, identificators, 9)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(12, ), delimiters, 8)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(12, ;, delimiters, 4)" ))
		return false;
	return true;
}

bool test13::run( std::istream&stream ){
	std::cout<<"Proverka na to chto reverse(s.begin(),s.end()); rasposnaetsa\n\
kak identificator reverse, razdelitel (, \n\
identificators s neraspoznanaya lexema ., identificator\n\
begin, razdelitel (, razdelitel ), razdelitel ,, \n\
identificator s, neraspoznanaya lexema .,\n\
identificator end, razdelitel (, razdelitel ),\n\
razdelitel ) i razdelitel ;";
	if (!is_equal_to_lex_conv(stream, "(13, reverse, identificators, 5)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(13, (, delimiters, 7)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(13, s, identificators, 5)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(13, ., unknown_lexems, 2)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(13, begin, identificators, 0)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(13, (, delimiters, 7)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(13, ), delimiters, 8)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(13, ,, delimiters, 0)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(13, s, identificators, 5)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(13, ., unknown_lexems, 2)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(13, end, identificators, 3)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(13, (, delimiters, 7)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(13, ), delimiters, 8)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(13, ), delimiters, 8)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(13, ;, delimiters, 4)" ))
		return false;
	return true;
}

bool test14::run( std::istream&stream ){
	std::cout<<"Proverka na to chto a=a-atoi(s.c_str()); rasposnaetsa kak\n\
identificator a, razdelitel =, identificators a\n\
razdelitel -, kluchevoe slovo atoi, razdelitel (,\n\
identificator s, neraspoznanaya lexema ., \n\
identificator c, neraspoznanaya lexema _, \n\
identificator str, razdelitel (, razdelitel ),\n\
razdelitel ), razdelitel ;";
	if (!is_equal_to_lex_conv(stream, "(14, a, identificators, 9)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(14, =, delimiters, 6)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(14, a, identificators, 9)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(14, -, delimiters, 1)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(14, atoi, keywords, 0)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(14, (, delimiters, 7)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(14, s, identificators, 5)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(14, ., unknown_lexems, 2)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(14, c, identificators, 0)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(14, _, unknown_lexems, 7)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(14, str, identificators, 4)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(14, (, delimiters, 7)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(14, ), delimiters, 8)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(14, ), delimiters, 8)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(14, ;, delimiters, 4)" ))
		return false;
	return true;
}

bool test15::run( std::istream&stream ){
	std::cout<<"Proverka na to chto s=\"������\"+\" ��������.\"; \n\
raspoznaetsya kak identificator s, razdelitel =,\n\
stroka \"������\", razdelitel +,\n\
stroka \" ��������.\" i razdelitel ;";
	if (!is_equal_to_lex_conv(stream, "(15, s, identificators, 5)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(15, =, delimiters, 6)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(15, \"������\", strings, 7)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(15, +, delimiters, 10)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(15, \" ��������.\", strings, 4)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(15, ;, delimiters, 4)" ))
		return false;
	return true;
}

bool test16::run( std::istream&stream ){
	std::cout<<"Proverka na to chto if(s!=\"Analis\n\
ne\n\
zavershen\") (to est stroka na neskolkih\n \
strokah) raspoznaetsya kak identificator if,\n\
razdelitel (,\n\
identificator s, neraspoznanaya lexema !,\n\
razdelitel = i stroka \"Analis\n\
ne\n\
zavershen\"";
	if (!is_equal_to_lex_conv(stream, "(16, if, identificators, 9)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(16, (, delimiters, 7)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(16, s, identificators, 5)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(16, !, unknown_lexems, 0)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(16, =, delimiters, 6)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(16, \"Analis\n\
ne\n\
zavershen\", strings, 0)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(18, ), delimiters, 8)" ))
		return false;
	return true;
}

bool test17::run( std::istream&stream ){
	std::cout<<"Proverka na to chto return 0; rasposnaetsa kak kluchevoe\n\
slovo return, konstanta 0 i razdelitel ;";
	if (!is_equal_to_lex_conv(stream,"(19, return, keywords, 1)"))
		return false;
	if (!is_equal_to_lex_conv(stream, "(19, 0, constants, 4)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(19, ;, delimiters, 4)" ))
		return false;
	return true;
}

bool test18::run( std::istream&stream ){
	std::cout<<"Proverka na to chto return 1; rasposnaetsa kak \n\
kluchevoe slovo return, konstanta 0\n\
i razdelitel ;";
	if (!is_equal_to_lex_conv(stream,"(20, return, keywords, 1)"))
		return false;
	if (!is_equal_to_lex_conv(stream, "(20, 1, constants, 5)" ))
		return false;
	if (!is_equal_to_lex_conv(stream, "(20, ;, delimiters, 4)" ))
		return false;
	return true;
}

bool test19::run( std::istream&stream ){
	std::cout<<"Proverka na to chto } rasposnaetsa kak razdelitel";
	if (!is_equal_to_lex_conv(stream,"(21, }, delimiters, 4)"))
		return false;
	return true;
}
