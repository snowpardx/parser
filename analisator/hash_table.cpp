#include "hash_table.h"

hash_table::hash_table(int size){
	this->size=size;
	this->table.resize(this->size);
}

int hash_table::hash( std::string& s ){
	int summ=0;
	for(auto i:s)
		summ+=i;
	int h=summ%this->size;
	h<0?h=-h:h;
	return h;
}

bool hash_table::find( std::string& s ){
	int h=hash(s);
	auto it=std::find(this->table[h].begin(),this->table[h].end(),s);
	if (it==this->table[h].end())
		return false;
	return true;
}


int hash_table::insert( std::string&s ){
	int h=hash(s);
	if (find(s))
		return h;
	this->table[h].push_back(s);
	return h;
}

std::ostream& operator<<(std::ostream&stream,hash_table&t){
	for(int i=0;i<t.size;i++){
		stream<<i<<":"<<std::endl;
		for (auto s:t.table[i])
			stream<<"\t"<<s<<std::endl;
	}
	return stream;
}