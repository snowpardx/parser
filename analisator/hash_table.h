#include "standart_libraries.h"
#pragma once
class hash_table
{
	int size;
	std::vector<std::list<std::string>> table;
	int hash(std::string& s);
public:
	hash_table(int size=11);
	bool find(std::string& s);
	int insert(std::string&s);
	friend std::ostream& operator<<(std::ostream&stream,hash_table&t);
};

