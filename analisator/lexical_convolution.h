#include "standart_libraries.h"
#pragma once
struct lexical_convolution{
	unsigned string_number,number_in_table;
	std::string token,table_name;
	lexical_convolution(unsigned _string_number,unsigned _number_in_table,std::string _token,std::string _table_name);
	friend std::ostream& operator<<(std::ostream&stream, lexical_convolution&lc);
};
