#pragma  once
#include "standart_libraries.h"
#include "analisator.h"
#include "syntax_analiser.h"
#define folder "C:\\Users\\alex\\Desktop\\analisator\\test_of_parser\\"

const char* getname(int i);
void print_table(int*** t){
	std::ofstream pt(folder"table.txt");
	int size=16-1;
	for (int row=size;row>=0;row--)
		for(int column=0;column<=row;column++)
			for (int i=1;i<how_many_non_term_is_for_cicles;i++){
				if(!t[row][column][i])
					pt<<"table["<<row<<"]["<<column<<"]["<<i<<"]="<<t[row][column][i]<<"  "<<getname(i)<<std::endl;
			}
}
void test1(){
	std::cout<<"�������� �� �� ��� int main(){ char a; a=1; return a; } ������������ ��� ������ ������������� �����"<<std::endl;
	analisator a(folder"test1_in.txt");
	a.start();
	std::ofstream syntax_out(folder"test1_out.txt");
	syntax_analiser SA;
	SA.start(a.get_list_of_lexical_covolution());
	syntax_out<<SA;
	if(SA.l.l_o_e.empty())
		std::cout<<"OK"<<std::endl;
	else
		std::cout<<"NOT_OK"<<std::endl;
	return;
}
void test2(){
	std::cout<<"�������� �� �� ��� � ������ int main(){ char a? a=1; return a; }\n\
			   �������� ��������������,��� ������ ? ��������� ;"<<std::endl;
	analisator a(folder"test2_in.txt");
	a.start();
	std::ofstream syntax_out(folder"test2_out.txt");
	syntax_analiser SA;
	SA.start(a.get_list_of_lexical_covolution());
	syntax_out<<SA;
	if(!SA.l.l_o_e.empty())
		if(SA.l.l_o_e[0].first.string_number==1)
			if(SA.l.l_o_e[0].first.token=="?"){
				std::cout<<"OK"<<std::endl;
				return;
			}
			std::cout<<"NOT_OK"<<std::endl;
			return;
}
void test3(){
	std::cout<<"���� 3 �����������"<<std::endl;
	analisator a(folder"test3_in.txt");
	a.start();
	std::ofstream syntax_out(folder"test3_out.txt");
	syntax_analiser SA;
	SA.start(a.get_list_of_lexical_covolution());
	syntax_out<<SA;
	if(SA.l.l_o_e.size()==5)
		if(SA.l.l_o_e[0].first.string_number==1)
			if(SA.l.l_o_e[0].first.token=="function")
				if(SA.l.l_o_e[1].first.string_number==1)
					if(SA.l.l_o_e[1].first.token=="(")
						if(SA.l.l_o_e[2].first.string_number==1)
							if(SA.l.l_o_e[2].first.token==")")
								if(SA.l.l_o_e[3].first.string_number==2)
									if(SA.l.l_o_e[3].first.token==",")
										if(SA.l.l_o_e[4].first.string_number==2)
											if(SA.l.l_o_e[4].first.token=="b"){
												std::cout<<"OK"<<std::endl;
												return;
											}
	std::cout<<"NOT_OK"<<std::endl;
	return;
}

void TESTS(){
	setlocale(LC_ALL,"Russian");
	test1();
	test2();
	test3();
}