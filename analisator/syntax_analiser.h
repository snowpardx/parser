#pragma once
#include "analisator.h"
#include "Tree.h"
#define term_prod 1
#define non_term_prod 2
#define how_many_non_term_is_for_cicles 73
//typedef std::map<std::string,int> my_map;
//struct my_pair{
//	std::pair<std::string,int> p;
//	friend std::ostream&operator<<(std::ostream&stream,my_pair&m);
//};
//class syntax_analiser
//{
//	std::list<lexical_convolution> lexical_convolutions;
//	class production{
//		std::string h,first_prod,follow_prod;
//	public:
//		int type(){return follow_prod.empty()?term_prod:non_term_prod;}
//		production(){}
//		production(std::string);
//		std::vector<std::string> get_prod_head_by_non_term(std::string,std::string);
//		std::vector<std::string> get_prod_head_by_term( std::string a);
//		std::vector<production> get_prods_by_head(std::string);
//		std::string head(){return h;}
//		std::string first(){return first_prod;}
//		std::string follow(){return follow_prod;}
//	};
//	struct list_of_err{
//		void create();
//		void poluchit_kronu(std::vector<my_pair>&v,Tree<my_pair>*tree);
//		syntax_analiser &SA;
//		list_of_err(syntax_analiser&_SA):SA(_SA){}
//		std::vector<std::pair<lexical_convolution,std::string>> l_o_e;
//		//friend std::ostream& operator<<(std::ostream &stream,list_of_err &l);
//	}l;
//	//void poluchit_kronu(std::vector<my_pair>&v);
//	friend std::ostream& operator<<(std::ostream &stream,list_of_err &l);
//	void create_tree();
//	Tree<my_pair>* create_tree_helper(std::string h,int row,int colummn,Tree<my_pair>* father=nullptr);
//public:
//	friend std::ostream &operator<<(std::ostream&stream,syntax_analiser& a);
//	/*struct non_terminal{
//	std::string NT;
//	int errors;
//	non_terminal(std::string _NT="",int _err=0):NT(_NT),errors(_err){};
//	};*/
//	void start(){
//		this->table.clear();
//		cock_yanger_kassami();
//		if (table[0][0].find("�������")!=table[0][0].end())
//			create_tree();
//	}
//	syntax_analiser(std::list<lexical_convolution> &_lexical_convolutions):lexical_convolutions(_lexical_convolutions),tree(nullptr),l(*this){};
//private:
//public:
//	std::vector<std::vector<my_map>> table;
//	Tree<my_pair> *tree;
//	static std::vector<production> productions;
//	static std::vector<production>init();
//	//??static std::vector<int> term_productions;
//	void cock_yanger_kassami();
//	void set_productions(int column,int row);//������������� ��� ��������� ��� ������� �������
//	my_map set_productions(my_map a,my_map b);//���� ��������� ��� 2 ����
//	my_map set_productions( my_map a);
//	my_map terminal_production(lexical_convolution& LC);//���������� ������������ ��������� ��� ����������� �������	
//};
#define ������� 1
#define �������2 2
#define �������3 3
#define ��������_������ 4
#define ��������_������2 5
#define ��������_������3 6
#define �����������_������� 7
#define ��������_������4 8
#define �����������_�������� 9
#define �����������_�������� 10
#define ��������_����� 11
#define RETURN 12
#define ��������_�����2 13
#define ������������� 14
#define ��������_�����3 15
#define �����_�_������� 16
#define ���_������� 17
#define �������� 18
#define ��������� 19
#define ���������2 20
#define ����� 21
#define ���������3 22
#define �����_��������� 23
#define ���������_��������� 24
#define �������� 25
#define ��������2 26
#define ��� 27
#define INT 28
#define CHAR 29
#define ������_�������� 30
#define ������ 31
#define ������_��������2 32
#define �����������_���������� 33 
#define ������� 34
#define ������_��������3 35
#define �����������_���������� 36
#define ������2 37
#define ������3 38
#define �������� 39
#define ��������2 40
#define ��������3 41
#define ����������_����� 42
#define ��������� 43
#define ����������_�����2 44
#define ����������_�����3 45
#define ����������_�����4 46
#define �����������_������� 47
#define ����������_��������� 48
#define ����������_���������2 49
#define ������ 50
#define ITOA 51
#define ����������_���������3 52
#define ����������_���������4 53
#define ����������_���������5 54
#define ����������_���������6 55
#define ATOI 56
#define �����_���������2 57
#define �����_���������3 58
#define �����_���������4 59
#define �����_���������5 60
#define ���� 61
#define ����� 62
#define �����_���������6 63
#define �����_���������7 64
#define ���������_���������2 65
#define ���������_���������4 66
#define ���������_���������3 67
#define ���������_���������5 68
#define ���������_���������6 69
#define ���������_���������7 70
#define �������������_NON_TERM 71
#define UNKNOWN_LEXEM 72

struct my_pair{
	std::pair<int,int> p;
	friend std::ostream&operator<<(std::ostream&stream,my_pair&m);
};

class syntax_analiser{
public:
	class production{
		int h,first_prod,follow_prod;
	public:
		int type(){return !follow_prod?term_prod:non_term_prod;}
		production(){}
		production(std::string);
		int head(){return h;}
		int first(){return first_prod;}
		int follow(){return follow_prod;}
	};
private:
	std::vector<production> get_prods_by_head(int);
	std::vector<int> get_prod_head_by_term(int a);
	std::vector<production> productions;
	std::vector<production>prod_init();
	std::vector<int>& get_prod_head_by_non_term(int,int);
	friend const char* getname(int i);
	static int get_prod_number(std::string s);
	std::vector<int> production_table[how_many_non_term_is_for_cicles][how_many_non_term_is_for_cicles];
	void set_productions(int row, int column);
public:
	syntax_analiser();
	void init();
	std::list<lexical_convolution> lexical_convolutions;
	void cock_yanger_kassami();
	Tree<my_pair> *tree;
	void create_tree();
	int ***table;
public:
	struct list_of_err{
		void create();
		void poluchit_kronu(std::vector<my_pair>&v,Tree<my_pair>*tree);
		syntax_analiser &SA;
		list_of_err(syntax_analiser&_SA):SA(_SA){}
		std::vector<std::pair<lexical_convolution,std::string>> l_o_e;
		friend std::ostream& operator<<(std::ostream &stream,list_of_err &l);
	}l;
	friend std::ostream &operator<<(std::ostream&stream,syntax_analiser& a);
	Tree<my_pair>* create_tree_helper( int h,int row,int column,Tree<my_pair>* father=nullptr );
	void del_table();
	void create_table();
	void fill_first_table_layer();
	void start(std::list<lexical_convolution> &_lexical_convolutions){
		lexical_convolutions=_lexical_convolutions;
		init();
		cock_yanger_kassami();
		if (table[0][0][�������]>-1)
			create_tree();
	}
};