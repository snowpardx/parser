#include "standart_libraries.h"
#include "hash_table.h"
#include "automaton.h"
#include "lexical_convolution.h"
#pragma once
class analisator
{
public:
analisator(const char*s);
~analisator();
void start();
friend std::ostream&operator<<(std::ostream&stream,analisator& a);
std::list<lexical_convolution> &get_list_of_lexical_covolution(){return lexical_convolutions;}
protected:
	std::list<lexical_convolution> lexical_convolutions;
	automaton autom;
	hash_table identificators,keywords,delimiters,constants,unknown_lexems,strings;
	void get_token();
	std::fstream f;
};
class not_open{};
