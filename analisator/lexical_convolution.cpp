#include "lexical_convolution.h"
lexical_convolution::lexical_convolution(unsigned _string_number,unsigned _number_in_table,std::string _token,std::string _table_name){
	string_number=_string_number;
	number_in_table=_number_in_table;
	token=_token;
	table_name=_table_name;
}
std::ostream& operator<<(std::ostream&stream, lexical_convolution&lc){
	stream<<"("<<lc.string_number<<", "<<lc.token<<", "<<lc.table_name<<", "<<lc.number_in_table<<")";
	return stream;
}