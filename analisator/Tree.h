#pragma once
template <typename X> struct Tree
	{
		Tree *father,*left,*right;
		X inf;
	friend std::ostream&operator<<(std::ostream& stream,Tree<X> t);
	Tree(Tree*_father=nullptr,Tree*_left=nullptr,Tree*_right=nullptr):father(_father),left(_left),right(_right){}
	void del();
};

template <typename X>
void Tree<X>::del(){
	if(this->left)
		left->del();
	if(this->right)
		right->del();
	if(this->father)
		this->father->left==this ? this->father->left=nullptr : this->father->right=nullptr;
	delete this;
}

template<typename X>
std::ostream&operator<<(std::ostream& stream,Tree<X> t){
	stream<<t.inf;
	if(t.left)
		stream<<*t.left;
	if(t.right)
		stream<<*t.right;
	return stream;
}
