#pragma  once
#include "standart_libraries.h"
#include "analisator.h"
#include "syntax_analiser.h"
void TESTS();
void main(int argc, const char *argv[]){
	try{
		setlocale(LC_ALL,"Russian");
		//debug_begin
		/*TESTS();
		return;*/
		//debug_end
		if (argc<2) {std::cerr<<"\n�� ����� �������� �������� ����.\n"; return;}
		analisator a(argv[1]);
		a.start();
		std::ofstream file;
		if (argc>2)
			file.open(argv[2],std::ios::out);
		else file.open("out.txt");
		if(!file)
			throw not_open();
		file<<a;

		std::ofstream syntax_out("syntax_output.txt");
		syntax_analiser SA;
		SA.start(a.get_list_of_lexical_covolution());
		syntax_out<<SA;
		std::cout<<"������ ��������."<<std::endl;
	}
	catch(not_open){
		std::cerr<<"���� ���������� �������.";
	}
}