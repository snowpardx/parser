﻿#include "analisator.h"


analisator::analisator(const char*s){
	this->f.open(s,std::ios::in);
	if(!f)
		throw not_open();
}


analisator::~analisator(){
	this->f.close();
}

void analisator::get_token(){
	auto id_tok=this->autom.get_token(this->f);
	if (id_tok.first=="identificators")
		this->lexical_convolutions.push_back(lexical_convolution(id_tok.third,this->identificators.insert(id_tok.second),id_tok.second,id_tok.first));
	if (id_tok.first=="strings")
		this->lexical_convolutions.push_back(lexical_convolution(id_tok.third,this->strings.insert(id_tok.second),id_tok.second,id_tok.first));
	else if(id_tok.first=="keywords")
		this->lexical_convolutions.push_back(lexical_convolution(id_tok.third,this->keywords.insert(id_tok.second),id_tok.second,id_tok.first));
	else if(id_tok.first=="delimiters")
		this->lexical_convolutions.push_back(lexical_convolution(id_tok.third,this->delimiters.insert(id_tok.second),id_tok.second,id_tok.first));
	else if(id_tok.first=="constants")
		this->lexical_convolutions.push_back(lexical_convolution(id_tok.third,this->constants.insert(id_tok.second),id_tok.second,id_tok.first));
	else if(id_tok.first=="unknown_lexems")
		this->lexical_convolutions.push_back(lexical_convolution(id_tok.third,this->unknown_lexems.insert(id_tok.second),id_tok.second,id_tok.first));
}

void analisator::start(){
	while(!this->f.eof())
		this->get_token();
}

std::ostream&operator<<(std::ostream&stream,analisator& a){
	stream<<"***********************\n"<<"LEXICAL_CONVOLUTIONS\n"<<"Общий вид (номер_строки, токен, таблица, номер в таблице)\n"
		<<"_______________________\n";
	for(auto it:a.lexical_convolutions)
		stream<<it<<std::endl;
	stream<<"_______________________\n\n\n\n\n";
	stream<<"***********************\n"<<"IDENTIFICATORS\n"<<"_______________________\n"<<a.identificators<<"_______________________\n\n\n\n\n";
	stream<<"***********************\n"<<"KEYWORDS\n"<<"_______________________\n"<<a.keywords<<"_______________________\n\n\n\n\n";
	stream<<"***********************\n"<<"DELIMITERS\n"<<"_______________________\n"<<a.delimiters<<"_______________________\n\n\n\n\n";
	stream<<"***********************\n"<<"STRINGS\n"<<"_______________________\n"<<a.strings<<"_______________________\n\n\n\n\n";
	stream<<"***********************\n"<<"CONSTANTS\n"<<"_______________________\n"<<a.constants<<"_______________________\n\n\n\n\n";
	stream<<"***********************\n"<<"UNKNOWN_LEXEMS\n"<<"_______________________\n"<<a.unknown_lexems<<"_______________________\n\n\n\n\n";
	return stream;
}