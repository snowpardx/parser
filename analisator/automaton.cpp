﻿#include "automaton.h"


void automaton::clear(){
	for(auto&i:this->states)
		i=false;
	this->states[0]=true;
	this->buffer.clear();
}

automaton::automaton(){
	this->string_number=1;
	this->states.push_back(true);
	for(int i=1;i<45;++i)
		this->states.push_back(false);
	this->final_states.push_back(2);
	this->final_states.push_back(6);
	this->final_states.push_back(10);
	this->final_states.push_back(16);
	this->final_states.push_back(20);
	this->final_states.push_back(24);
	this->final_states.push_back(25);
	this->final_states.push_back(26);
	this->final_states.push_back(27);
	this->final_states.push_back(28);
	this->final_states.push_back(29);
	this->final_states.push_back(30);
	this->final_states.push_back(31);
	this->final_states.push_back(32);
	this->final_states.push_back(33);
	this->final_states.push_back(34);
	this->final_states.push_back(35);
	this->final_states.push_back(36);
	this->final_states.push_back(37);
	this->final_states.push_back(38);
	this->final_states.push_back(39);
	this->final_states.push_back(40);
	this->final_states.push_back(41);
	this->final_states.push_back(42);
	this->final_states.push_back(43);
	this->final_states.push_back(44);
}

std::vector<int> automaton::valid_automaton(){
	std::vector<int> rv;
	for (auto state:this->final_states)
		if (this->states[state])
			rv.push_back(state);
	return rv;
}

std::string automaton::recognize_token(){
	auto st=valid_automaton();
	if(st.empty())
		return "unknown_lexems";
	if ((st[0]==6)||(st[0]==10)||(st[0]==16)||(st[0]==20)||(st[0]==24))
		return "keywords";
	if (st[0]==2)
		return "strings";
	if (st[0]==33)
		return "constants";
	if((st[0]>24)&&(st[0]<33))
		return "identificators";
	return "delimiters";
}

bool automaton::get_next_state(unsigned char simbol){
	auto rv=this->states;
	bool is_get=false;
	for(int i=0;i<44;i++){
		if (this->states[i]){
			auto sost=this->theta_function(simbol,i);
			if(!is_get) 
				if (sost[0]!=-1)is_get=true;
			rv[i]=false;
			for(auto t:sost){
				if (sost[0]==-1){
					break;
				}
				rv[t]=true;
			}
		}
	}
	if(!is_get)
		return false;
	this->states=rv;
	return true;
}

std::vector<int> automaton::theta_function(unsigned char simbol,int sostoyanie){
	std::vector<int> rv;
	if ((sostoyanie==0)&&(simbol=='\"')) {rv.push_back(1); return rv;}
	if ((sostoyanie==1)&&(simbol=='\"')) {rv.push_back(2); return rv;}
	if ((sostoyanie==1)&&(simbol>=1)&&(simbol<=255)) {rv.push_back(3); return rv;}
	if ((sostoyanie==3)&&(simbol=='\"')) {rv.push_back(2); return rv;}
	if ((sostoyanie==3)&&(simbol>=1)&&(simbol<=255)) {rv.push_back(3); return rv;}
	if ((sostoyanie==0)&&(simbol=='i')) {rv.push_back(4); rv.push_back(17); rv.push_back(25); return rv;}
	if ((sostoyanie==17)&&(simbol=='t')) {rv.push_back(18); return rv;}
	if ((sostoyanie==18)&&(simbol=='o')) {rv.push_back(19); return rv;}
	if ((sostoyanie==19)&&(simbol=='a')) {rv.push_back(20); return rv;}
	if ((sostoyanie==4)&&(simbol=='n')) {rv.push_back(5); return rv;}
	if ((sostoyanie==5)&&(simbol=='t')) {rv.push_back(6); return rv;}
	if ((sostoyanie==0)&&(simbol=='c')) {rv.push_back(7); rv.push_back(25); return rv;}
	if ((sostoyanie==7)&&(simbol=='h')) {rv.push_back(8); return rv;}
	if ((sostoyanie==8)&&(simbol=='a')) {rv.push_back(9); return rv;}
	if ((sostoyanie==9)&&(simbol=='r')) {rv.push_back(10); return rv;}
	if ((sostoyanie==0)&&(simbol=='r')) {rv.push_back(11); rv.push_back(25); return rv;}
	if ((sostoyanie==11)&&(simbol=='e')) {rv.push_back(12); return rv;}
	if ((sostoyanie==12)&&(simbol=='t')) {rv.push_back(13); return rv;}
	if ((sostoyanie==13)&&(simbol=='u')) {rv.push_back(14); return rv;}
	if ((sostoyanie==14)&&(simbol=='r')) {rv.push_back(15); return rv;}
	if ((sostoyanie==15)&&(simbol=='n')) {rv.push_back(16); return rv;}
	if ((sostoyanie==0)&&(simbol=='a')) {rv.push_back(21); rv.push_back(25); return rv;}
	if ((sostoyanie==21)&&(simbol=='t')) {rv.push_back(22); return rv;}
	if ((sostoyanie==22)&&(simbol=='o')) {rv.push_back(23); return rv;}
	if ((sostoyanie==23)&&(simbol=='i')) {rv.push_back(24); return rv;}
	if ((sostoyanie==0)&&(( (simbol>='A')&&(simbol<+'Z'))||((simbol>='a')&&(simbol<='z')))) {rv.push_back(25); return rv;}
	if ((sostoyanie==25)&&(((simbol>='0')&&(simbol<='9'))||( (simbol>='A')&&(simbol<='Z') )||((simbol>='a')&&(simbol<='z')))) 
	{rv.push_back(26); return rv;}
	if ((sostoyanie==26)&&(((simbol>='0')&&(simbol<='9'))||( (simbol>='A')&&(simbol<='Z') )||((simbol>='a')&&(simbol<='z')))) 
	{rv.push_back(27); return rv;}
	if ((sostoyanie==27)&&(((simbol>='0')&&(simbol<='9'))||( (simbol>='A')&&(simbol<='Z') )||((simbol>='a')&&(simbol<='z')))) 
	{rv.push_back(28); return rv;}
	if ((sostoyanie==28)&&(((simbol>='0')&&(simbol<='9'))||( (simbol>='A')&&(simbol<='Z') )||((simbol>='a')&&(simbol<='z')))) 
	{rv.push_back(29); return rv;}
	if ((sostoyanie==29)&&(((simbol>='0')&&(simbol<='9'))||( (simbol>='A')&&(simbol<='Z') )||((simbol>='a')&&(simbol<='z')))) 
	{rv.push_back(30); return rv;}
	if ((sostoyanie==30)&&(((simbol>='0')&&(simbol<='9'))||( (simbol>='A')&&(simbol<='Z') )||((simbol>='a')&&(simbol<='z')))) 
	{rv.push_back(31); return rv;}
	if ((sostoyanie==31)&&(((simbol>='0')&&(simbol<='9'))||( (simbol>='A')&&(simbol<='Z') )||((simbol>='a')&&(simbol<='z')))) 
	{rv.push_back(32); return rv;}
	if ((sostoyanie==0)&&(simbol>='0')&&(simbol<='9')) {rv.push_back(33); return rv;}
	if ((sostoyanie==33)&&(simbol>='0')&&(simbol<='9')) {rv.push_back(33); return rv;}
	if ((sostoyanie==0)&&(simbol=='(')) {rv.push_back(34); return rv;}
	if ((sostoyanie==0)&&(simbol==')')) {rv.push_back(35); return rv;}
	if ((sostoyanie==0)&&(simbol=='{')) {rv.push_back(36); return rv;}
	if ((sostoyanie==0)&&(simbol=='}')) {rv.push_back(37); return rv;}
	if ((sostoyanie==0)&&(simbol=='+')) {rv.push_back(38); return rv;}
	if ((sostoyanie==0)&&(simbol=='-')) {rv.push_back(39); return rv;}
	if ((sostoyanie==0)&&(simbol==';')) {rv.push_back(40); return rv;}
	if ((sostoyanie==0)&&(simbol=='[')) {rv.push_back(41); return rv;}
	if ((sostoyanie==0)&&(simbol==']')) {rv.push_back(42); return rv;}
	if ((sostoyanie==0)&&(simbol=='=')) {rv.push_back(43); return rv;}
	if ((sostoyanie==0)&&(simbol==',')) {rv.push_back(44); return rv;}
	rv.push_back(-1);
	return rv;
}

triple<std::string,std::string,unsigned> automaton::get_token( std::istream& stream ){
	this->clear();
	bool bad_buffer=false;
	unsigned r_str_numb=this->string_number;
	triple<std::string,std::string,unsigned> rp;
	for(;;){
		unsigned char c=stream.peek();//иначе при использовании типа char кирилические символы будут иметь отрицательный номер
		char c1;
		if (c=='\n'){
			this->string_number++;
		if (this->buffer.empty())
				r_str_numb=this->string_number;
		}
		if (!(this->states[3]||this->states[1]))//чтобы не удалялись пробелы из строковой константы
			while((c==' ')||(c=='\t')||(c=='\n')){//для всего остального они бесполезны
				if (!this->buffer.empty()){
					rp.first=this->recognize_token();
					rp.second=buffer;
					rp.third=r_str_numb;
					stream.get(c1);
					return rp;
				}
				stream.get(c1);
				c=stream.peek();
			}
			bool state_about=get_next_state(c);
			if (state_about)
				if (!bad_buffer){
					this->buffer.push_back(c);
					stream.get(c1);
				}
				else{
					rp.second=this->buffer;
					rp.first="unknown_lexems";//this->recognize_token();
					rp.third=r_str_numb;
					return rp;
				}
			else if (!state_about){
				bad_buffer=true;
				rp.first=this->recognize_token();
				if (!stream){
					this->buffer.pop_back();
					rp.second=this->buffer;
					rp.third=r_str_numb;
					return rp;
				}
				if (rp.first!="unknown_lexems"){
					rp.second=this->buffer;
					rp.third=r_str_numb;
					return rp;
				}
				this->states[0]=true;
				this->buffer.push_back(c);
				stream.get(c1);
			}
	}
}
