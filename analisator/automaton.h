//#include "hash_table.h"
#include "standart_libraries.h"
#pragma once
template <typename X,typename Y,typename Z> struct triple{
	X first;
	Y second;
	Z third;
};
class automaton{
	std::vector<bool> states;
	std::vector<int> final_states;
	std::string buffer;
	unsigned string_number;
	void clear();
	std::vector<int> valid_automaton();//returns final states where automaton is
	bool get_next_state(unsigned char simbol);
	std::string recognize_token();
	std::vector<int> theta_function(unsigned char simbol,int sostoyanie);
public:
	automaton();
	triple<std::string,std::string,unsigned> get_token(std::istream& stream);
};