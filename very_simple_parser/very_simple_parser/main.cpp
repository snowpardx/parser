#include <iostream>
#include <fstream>
#include <map>
#include <vector>
#include <string>
using namespace std;
void main(int argc, char*argv[]){
	ifstream inputFileStream(argv[1]);
	char s[180];
	vector<string> vs;
	while(1){
		inputFileStream.getline(s,80);
		if(!inputFileStream)
			break;
		vs.push_back(s);
	}
	map<string,int> m;
	for(auto i:vs){
		i.erase(i.begin(),i.begin()+24);
		i.erase(i.size()-5,i.size()-1);
		for (int k=0;i.find(" ")!=string::npos;)
		{
			int cur=k;
			k=i.find(" ",k);
			m[i.substr(cur,k)]=0;
		}
	}
	int np=1;
	ofstream outputFileStream("out.txt");
	for (auto i=m.begin();i!=m.end();np++)
		outputFileStream<<"#define "<<i->first<<" "<<np<<endl;
}